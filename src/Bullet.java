import bagel.Image;
import bagel.util.Point;

public class Bullet {
    private final Image image = new Image("res/images/bullet.png");

    private Point pos;

    private Zombie deszom;
    private double desX;
    private double desY;

    public static final double STEP_SIZE = 25;

    public Bullet(double x, double y, double desX, double desY, Zombie deszom) {
        this.pos = new Point(x, y);
        this.desX = desX;
        this.desY = desY;
        this.deszom = deszom;
    }

    public Point getPos() {
        return pos;
    }

    public void draw() {
        image.drawFromTopLeft(pos.x, pos.y);
    }

    public void update() {
        if (deszom.shooted(this)) {
            Player.setbullet();
        } else {
            this.pos = new Point(this.pos.x + STEP_SIZE * this.desX, this.pos.y + STEP_SIZE * this.desY);

        }
    }
}
