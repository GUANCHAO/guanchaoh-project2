import bagel.DrawOptions;
import bagel.Font;
import bagel.Image;
import bagel.util.Colour;
import bagel.util.Point;
import bagel.util.Vector2;

import java.util.ArrayList;

public class Player {

    public static final String FILENAME = "res/images/player.png";

    public static final double STEP_SIZE = 10;

    private static final int LOWENERGY = 3;

    private static final Vector2 ZERO_VECTOR = new Vector2(0, 0);

    private final Font FONT = new Font("res/font/DejaVuSans-Bold.ttf", 20);
    private final DrawOptions OPT = new DrawOptions();

    private final Image image;

    private Point pos;
    private double directionX;
    private double directionY;

    private int energy;
    private static int isbullet = 0;
    private static Bullet bullet;

    public Player(double x, double y, int energy) {
        this.image = new Image(FILENAME);
        this.pos = new Point(x, y);
        this.energy = energy;
    }

    public Point getPos() {
        return this.pos;
    }

    public int getEnergy() {
        return this.energy;
    }

    public void pointTo(Point dest) {
        this.directionX = dest.x - this.pos.x;
        this.directionY = dest.y - this.pos.y;
        normalizeD();
    }

    public void normalizeD() {
        double len = Math.sqrt(Math.pow(this.directionX, 2) + Math.pow(this.directionY, 2));
        this.directionX /= len;
        this.directionY /= len;
    }

    public void update(ShadowTreasure tomb) {
        Zombie zombie = this.nearstZombie(tomb.getZombie());
        Sandwich sandwich = this.nearstSandwich(tomb.getSandwich());
        if (zombie == null) {
            pointTo(tomb.getTreasure().getPos());
            tomb.setEndOfGame(true);
        } else {
            if (zombie.meets(this) || isbullet == 1) {
                shoot(zombie.getPos().x, zombie.getPos().y, zombie);
                tomb.setEndOfGame(true);
            } else if (sandwich.meets(this)) {
                eatSandwich();
                sandwich.setVisible(false);
            }

            if (this.energy >= LOWENERGY) {
                pointTo(this.nearstZombie(tomb.getZombie()).getPos());
            } else {
                pointTo(this.nearstSandwich(tomb.getSandwich()).getPos());
            }
        }
        this.pos = new Point(this.pos.x + STEP_SIZE * this.directionX, this.pos.y + STEP_SIZE * this.directionY);
    }

    public void render() {
        image.drawFromTopLeft(pos.x, pos.y);
        FONT.drawString("energy: " + energy, 20, 760, OPT.setBlendColour(Colour.BLACK));
    }

    public void eatSandwich() {
        energy += 5;
    }

    public void reachZombie() {
        energy -= 3;
    }

    public double dis(Point pos) {
        //return Math.sqrt(Math.pow(this.pos.x-x,2) + Math.pow(this.pos.y-y,2));
        return this.pos.distanceTo(pos);
    }

    public Zombie nearstZombie(ArrayList<Zombie> zombielist) {
        int max = Integer.MAX_VALUE;
        Zombie nearst = null;
        Point zombiepos;
        for (Zombie zombie : zombielist) {
            zombiepos = zombie.getPos();
            if (this.dis(zombiepos) < max)
                nearst = zombie;
        }
        return nearst;
    }

    public Sandwich nearstSandwich(ArrayList<Sandwich> Sandwichlist) {
        int max = Integer.MAX_VALUE;
        Sandwich nearst = null;
        Point sandwichpos;
        for (Sandwich sandwich : Sandwichlist) {
            sandwichpos = sandwich.getPos();
            if (this.dis(sandwichpos) < max)
                nearst = sandwich;
        }
        return nearst;
    }

    public void shoot(double desx, double desy, Zombie deszom) {
        if (isbullet == 0) {
            this.isbullet = 1;
            Bullet bullet = new Bullet(this.pos.x, this.pos.y, desx, desy, deszom);
            bullet.draw();
            this.energy -= 3;
        } else {
            bullet.update();
            bullet.draw();
        }

    }

    public static void setbullet() {
        isbullet = 0;
        bullet = null;
    }
}
