import bagel.*;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ShadowTreasure extends AbstractGame {

    private final Image Backgroubd = new Image("res/images/brackground.png");
    public static final int CLOSENESS = 150;

    private static DecimalFormat df = new DecimalFormat("0.00");

    private final int TICK_CYCLE = 10;
    private int tick;

    private Player player;
    private Treasure treasure;
    private ArrayList<Sandwich> sandwitch = new ArrayList<Sandwich>();
    private ArrayList<Zombie> zombie = new ArrayList<Zombie>();

    private boolean endOfGame;

    public ShadowTreasure() throws IOException {
        this.loadEnvironment("res/IO/environment.csv");
        this.tick = 1;
        this.endOfGame = false;
        System.out.println(player.getPos().x + "," + player.getPos().y + "," + player.getEnergy());
    }

    public ArrayList<Sandwich> getSandwich() {
        return sandwitch;
    }

    public ArrayList<Zombie> getZombie() {
        return zombie;
    }

    public Treasure getTreasure() {
        return treasure;
    }

    public void setEndOfGame(boolean endOfGame) {
        this.endOfGame = endOfGame;
    }

    public void loadEnvironment(String filename) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                String type = parts[0];
                type = type.replaceAll("[^a-zA-Z0-9]", "");
                int x = Integer.parseInt((parts[1]));
                int y = Integer.parseInt((parts[2]));
                switch (type) {
                    case "Player":
                        this.player = new Player(x, y, Integer.parseInt(parts[3]));
                        break;
                    case "Zombie":
                        this.zombie.add(new Zombie(x, y));
                        break;
                    case "Sandwich":
                        this.sandwitch.add(new Sandwich(x, y));
                        break;
                    case "Treasure":
                        this.treasure = new Treasure(x, y);
                    default:
                        throw new BagelError("Unknown type:" + type);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    protected void update(Input input) {
        if (this.endOfGame || input.wasPressed(Keys.ESCAPE)) {
            Window.close();
        } else {
            Backgroubd.drawFromTopLeft(0, 0);
            if (tick > TICK_CYCLE) {
                player.update(this);
                tick = 1;
                System.out.println(df.format(player.getPos().x) + "," + df.format(player.getPos().y) + "," + player.getEnergy());
            }
            tick++;
            for (Sandwich sand : sandwitch) {
                sand.draw();
            }
            for (Zombie zom : zombie) {
                zom.draw();
            }
            treasure.draw();
            player.render();
        }
    }

    public static void main(String[] args) throws IOException {
        ShadowTreasure game = new ShadowTreasure();
        game.run();
    }


}
