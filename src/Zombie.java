import bagel.Image;
import bagel.util.Point;

public class Zombie {

    // image and type
    private final Image image = new Image("res/images/zombie.png");

    // render position
    private Point pos;
    private int vis = 1;

    public Zombie(double x, double y) {
        this.pos = new Point(x, y);
    }

    public Point getPos() {
        return pos;
    }

    // render image
    public void draw() {
        if (this.vis == 1)
            image.drawFromTopLeft(pos.x, pos.y);
    }

    public boolean meets(Player player) {
        boolean hasMet = false;
        double distanceToPlayer = player.getPos().distanceTo(pos);
        if (distanceToPlayer < ShadowTreasure.CLOSENESS) {
            hasMet = true;
        }
        return hasMet;
    }

    public boolean shooted(Bullet bullet) {
        boolean hasshot = false;
        double distanceToBullet = bullet.getPos().distanceTo(pos);
        if (distanceToBullet < 50) {
            hasshot = true;
            this.vis = 0;
        }
        return hasshot;
    }
}
